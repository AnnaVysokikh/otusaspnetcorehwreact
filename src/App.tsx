import { BrowserRouter } from 'react-router-dom';
import Layout from './Components/Layout/Layout';
import { Navigation } from './Navigation';

function App() {
  return (
    <div className="App">
        <BrowserRouter>
          <Layout>
            <Navigation />
          </Layout>
        </BrowserRouter>
    </div>
  );
}

export default App;

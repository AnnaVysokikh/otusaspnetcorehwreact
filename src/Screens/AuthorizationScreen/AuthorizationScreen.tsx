import React, { FC } from 'react';
import { Page } from '../../Components/Page';
import s from './AuthorizationScreen.sass';

export const AuthorizationScreen: FC = () => {
  return (
    <Page title="Авторизация" className={s.root}>
      Форма для авторизации
    </Page>
  );
};

export default AuthorizationScreen;
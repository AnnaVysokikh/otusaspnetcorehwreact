import React, { FC } from 'react';
import { Page } from '../../Components/Page';
import s from './Home.module.sass';
import CatFactForm from '../../Components/Forms/CatFactForm/CatFactForm';
import Error from '../../Components/Error/Error';
import { RootState } from '../../reduxToolkit/store';
import { useSelector } from 'react-redux';

export const Home: FC = () => {
  const isError = useSelector<RootState, boolean>(state => state.catFactSlice.isError);
  return (
    <Page title="Главная" className={s.root}>
      <div>ДЗ</div>
      <CatFactForm />
      {isError && <Error /> }
    </Page>
  );
};

export default Home;

import React, { FC } from 'react';
import s from './NotFound.module.sass';
import { Page } from '../../Components/Page';

export const NotFound: FC = () => {
    return (
      <Page title="NotFound" className={s.root}>
        Ошибочка
      </Page>
    );
  };
  
  export default NotFound;
import React, { FC } from 'react';
import { Page } from '../../Components/Page';
import s from './RegistrationScreen.sass';

export const RegistrationScreen: FC = () => {
  return (
    <Page title="Регистрация" className={s.root}>
      Форма регистрации
    </Page>
  );
};

export default RegistrationScreen;
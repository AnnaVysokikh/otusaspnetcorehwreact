import React from 'react';
import { Route, Routes } from 'react-router-dom';
import HomeScreen from '../Screens/HomeScreen';
import NotFound from '../Screens/NotFound';
import RegistrationScreen from '../Screens/RegistrationScreen';
import AuthorizationScreen from '../Screens/AuthorizationScreen';

export const Navigation: React.FC = () => (
    <Routes>
      <Route path="/" element={<HomeScreen />} />
      <Route path="/signin" element={<AuthorizationScreen />} />
      <Route path="/signup" element={<RegistrationScreen />} />
      <Route path="*" element={<NotFound />} />
    </Routes>
);

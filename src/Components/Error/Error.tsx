import s from './Error.module.sass';
import { useSelector } from 'react-redux';
import { RootState } from '../../reduxToolkit/store';

const Error = () => {
    var err = useSelector<RootState, string>(state => state.catFactSlice.errorText);
    return (
      <div className={s.content}>
        {err}
      </div>
    )
}

export default Error;
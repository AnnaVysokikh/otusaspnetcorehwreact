import { SubmitHandler, useForm } from 'react-hook-form';
import s from './CatFactForm.module.sass';
import { useDispatch } from 'react-redux'
import { ThunkDispatch } from 'redux-thunk';
import { UnknownAction } from '@reduxjs/toolkit';
import { setCatFact, setError } from '../../../reduxToolkit/catFactSlice';
import axios from 'axios';

interface CatFactFormProps {
  newUrl: string;
};
const URL = "https://catfact.ninja/fact";

const CatFactForm = () => {
    const { 
      register,
      handleSubmit,
      formState: { errors },
    } = useForm<CatFactFormProps>({mode: 'onBlur'});

    interface StateProps {
      url: string,
      catFact: string
    }
    type AppDispatch = ThunkDispatch<StateProps, any, UnknownAction>;
    const dispatch: AppDispatch = useDispatch();

    const onSend: SubmitHandler<CatFactFormProps> = (value): void => {
      console.log(value.newUrl)
      var newUrl = value.newUrl
      if (URL !== newUrl) {
        var error = "Введен плохой URL";
        dispatch(setError({ error }))
      } else {
          axios.get(newUrl)
                .then(response => {
                  var newFact = response.data['fact']
                  dispatch(setCatFact({ newFact }))
                })
                .catch(error => {
                  var error = error.message
                  dispatch(setError({ error }))
                });
          }
    }

    return (
      <form className={s.form} onSubmit={handleSubmit(onSend)}>
        <div className={s.field}>
          <label className={s.label}>Введите URL</label>
          <input
            defaultValue="https://catfact.ninja/fact"
            className={s.input_pass}

            type="text"
            placeholder="Введите URL"
            {...register('newUrl', { required: "Обязательное поле"})}
          />
          <label className={s.error_label}>{errors.newUrl?.message}</label>
        </div>
        <button className={s.button_send} type="submit">
          Отправить
        </button>
      </form>
    )
}
export default CatFactForm;
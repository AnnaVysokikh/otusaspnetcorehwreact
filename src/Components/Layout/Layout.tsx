import React from 'react';
import s from './Layout.module.sass';
import Header from '../Header/Header';
import CatFact from '../CatFact/CatFact';

type LayoutProps = {
    children?: React.ReactNode;
  };

  const Layout = ({ children }: LayoutProps) => {
    return (
      <div className={s.layout}>
        <Header />
        <div className={s.content}>{children}</div>
        <CatFact />
       </div>
    );
  };
  
  export default Layout;
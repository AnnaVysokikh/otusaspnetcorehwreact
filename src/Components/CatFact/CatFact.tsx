import { FC, useEffect } from 'react';
import s from './CatFact.module.sass';
import { useSelector } from 'react-redux';
import { RootState } from '../../reduxToolkit/store';

const CatFact: FC = () => {
  const catFact = useSelector<RootState, string>(state => state.catFactSlice.catFact);
  useEffect(()=>{
    console.log("Обновляю компонент CatFact")
  })
  return (
    <div className={s.content}>
      {catFact}
    </div>
  );
};

export default CatFact;
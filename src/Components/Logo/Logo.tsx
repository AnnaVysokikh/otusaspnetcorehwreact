import { Component, ReactNode } from 'react';
import RocketIcon from '@mui/icons-material/Rocket';
import s from './Logo.module.sass';

export class Logo extends Component<{}> {
  render(): ReactNode {
  return (
    <div className={s.logo}>
      <RocketIcon />
    </div>
)}};

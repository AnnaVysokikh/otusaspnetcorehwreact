import React from 'react';
import s from './Header.module.sass';
import { NavLink, NavLinkProps } from 'react-router-dom';
import cn from 'clsx';
import {Logo} from '../Logo/Logo';

type HeaderProps = {
  children?: React.ReactNode;
};

export const getClassName: NavLinkProps['className'] = ({ isActive }) => cn(s.link, isActive && s.active);
const Header = ({ children }: HeaderProps) => {
  return (
    <div className={s.header}>
      <Logo/>
      <div className={s.menu}>
        <NavLink className={getClassName} to="/">
          HomePage 
        </NavLink>
        <NavLink className={getClassName} to="/signin">
          Login 
        </NavLink>
        <NavLink className={getClassName} to="/signup">
          Register 
        </NavLink>
      </div>
      {children}
    </div>
  );
};

export default Header;
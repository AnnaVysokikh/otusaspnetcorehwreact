import { configureStore } from '@reduxjs/toolkit';
import thunkMiddleware from 'redux-thunk';
import catFactSlice from './catFactSlice';

export const store = configureStore({
  reducer: {
    catFactSlice,
  },
//   middleware: [thunkMiddleware],
});

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;

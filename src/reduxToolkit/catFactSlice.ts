import { createSlice } from '@reduxjs/toolkit';

export interface StateProps {
  url: string,
  catFact: string,
  errorText:string,
  isError: boolean
}

const initialState: StateProps = {
    url: "https://catfact.ninja/fact",
    catFact: "Отправьте запрос на главной странице",
    errorText: "",
    isError: false
};

export const catFactSlice = createSlice({
  name: 'catFactSlice',
  initialState,
  reducers: {
    setCatFact: (state: StateProps, action: { payload: { newFact: string } }) => {
        const { newFact } = action.payload;
        state.catFact = newFact;
        state.isError =  false;
        console.log(newFact)
    },
    setError:  (state: StateProps, action: { payload: { error: string } }) => {
      const { error } = action.payload;
      state.errorText = error;
      state.isError =  true;
    }
  },
});

export const { setCatFact, setError } = catFactSlice.actions;
export default catFactSlice.reducer;